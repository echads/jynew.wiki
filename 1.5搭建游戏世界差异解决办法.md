# 搭建游戏世界差异解决办法

前面我们有提到DOS版金群的运行逻辑，如果完全按照原版游戏来“移植”会存在一些问题，这里我们来讲这些问题的解决思路。


## 格子和触发器

由于DOS版是基于“格子”（含坐标）的，而重置版是基于3D位置的。所以最基础的触发概念和脚本编写逻辑并不能通用。

DOS版中，格子可以基于当前玩家的坐标来进行判断。包括脚本中修改格子的显示内容，也可以通过传入坐标、修改图像来进行，而在3D版中做不到。

如典型的出口位置，DOS版只用卡在某个坐标点即可

![图片17](https://user-images.githubusercontent.com/7448857/119357049-b412fb00-bcd9-11eb-88d8-ccbc733b8391.png)

而3D版，我们需要拉出整个“碰撞区域”

![图片18](https://user-images.githubusercontent.com/7448857/119357068-baa17280-bcd9-11eb-9584-e29864752793.png)

注意碰撞区域我们是使用Collider来进行碰撞检测，一般使用Box Collider

![图片19](https://user-images.githubusercontent.com/7448857/119357112-c4c37100-bcd9-11eb-8bee-38e105a2c833.png)

具体的碰撞范围可以在Unity的Hierarchy窗口中双击触发器，然后在Scene中预览到。

## 如何新建触发器？

- 在unity对应的场景中，Level/Triggers下新建一个GameObject，取名需跟原来触发器一致。
- 添加Collider组件（Add Component-选择Collider类型）
- 拖到地图上对应位置，并设置好Collider相关参数
- 添加对应触发器类型（BigMapZone或GameEvent），填写参数
- 启动游戏进行测试

## DOS版含坐标类指令的重置方式

理论上ModifyEvent（涉及到修改贴图或触发器位置的实现）和SetScenceMap（修改地图贴图）都需要在重置版中重新实现。
所以需要在lua里对该类事件进行标记，我们后续在unity里编程实现它们。

是什么原理？

实现拆解：
我们以DOS版在灵蛇岛营救王难姑剧情为例：玩家可以在灵蛇岛击败金花婆婆后打开一个小屋的门，进入小屋与王难姑对话，王难姑会回到蝴蝶谷。玩家到蝴蝶谷可以看到胡青牛和王难姑夫妇，与胡青牛对话即可招募，招募到胡青牛后，与王难姑对话亦可招募。

### 1、设置灵蛇岛上的门
查看灵蛇岛对应的DOS版配置

![图片20](https://user-images.githubusercontent.com/7448857/119357471-313e7000-bcda-11eb-9d16-9b2e12f71549.png)


发现与金花婆婆对话是触发98号事件，对应lua文件如下（jyx2/data/lua/jygame/ka98.lua）

以下红色部分为原代码，黄色部分为重置版需要手动添加的代码，下面作解释：

![图片21](https://user-images.githubusercontent.com/7448857/119357491-37cce780-bcda-11eb-9fed-72d79c8f1545.png)

语句SetSceneMap(-2,1,21,17,0)（1所示红色部分）的意思是
修改当前场景（-2）的建筑层（1）的坐标（21,17）为空（0）

通过查看编辑器，看到（21,17）位置是

![图片22](https://user-images.githubusercontent.com/7448857/119357543-461b0380-bcda-11eb-9054-17747457cc82.png)


可以判断此指令作用为将门设置为空，从而可以让玩家进入房间。

那么，我们可以unity场景中将两个门摆上，层级放置在Level/Dynamic下，命名为Door_01和Door_02

![图片23](https://user-images.githubusercontent.com/7448857/119357617-5b902d80-bcda-11eb-8e7f-8c3e256a6d82.png)


由于该门需要阻挡玩家的行动路线，我们给其增加NavMeshObstacle组件

![图片24](https://user-images.githubusercontent.com/7448857/119357639-61860e80-bcda-11eb-820f-d06cbbb2bfc6.png)
![图片25](https://user-images.githubusercontent.com/7448857/119357655-65b22c00-bcda-11eb-8c81-68c16e41251b.png)

可以通过调整NavMeshObstacle中的参数形状（Shape）和相关坐标，来确定在场景内的具体阻挡几何体。（将以此几何体拦住主角）

在98号事件的lua脚本下，补充两行代码

```
        jyx2_ReplaceSceneObject("", "Dynamic/Door_01", "");  
        jyx2_ReplaceSceneObject("", "Dynamic/Door_02", "");  
```

意思为将这两个门设置为空。

指令解释：
这里我们新提供了一种指令：jyx2_ReplaceSceneObject，作用是替换场景内物体的方法
三个参数分别为 场景ID（为空则是当前场景）、替换物品的全路径（对应scene中hierarchy中Level节点下的层级）、是否隐藏（为””则隐藏，为”1”则是显示）

### 2、与王难姑对话，王难姑移动到蝴蝶谷

查原版得知王难姑的对话触发是103号事件，我们打开对应lua文件
![图片26](https://user-images.githubusercontent.com/7448857/119358020-c5a8d280-bcda-11eb-877a-1b964a4e5a99.png)
![图片27](https://user-images.githubusercontent.com/7448857/119358023-c93c5980-bcda-11eb-935d-4e70e745f932.png)

通过分析，我们得知红色部分两句ModifyEvent代码分别是移除王难姑、在44号场景（蝴蝶谷）中显示王难姑

我们可以分别加上对应的jyx2_ReplaceSceneObjet指令进行实现。

所以我们需要在蝴蝶谷场景中将王难姑加上，并且默认设置为不显示（取消以下勾选部分）

![图片28](https://user-images.githubusercontent.com/7448857/119358084-d8230c00-bcda-11eb-8c31-d0c370aca0dc.png)
![图片29](https://user-images.githubusercontent.com/7448857/119358071-d5c0b200-bcda-11eb-9500-f4245443ba9c.png)

那么在执行到103号事件对应两条新增代码时，就会将原本在灵蛇岛的“Level/NPC/王难姑”隐藏，将44号场景（蝴蝶谷）中的“Level/NPC/王难姑”显示。

注：jyx2_ReplaceSceneObjet所有调整过的状态都是跟当前游戏存档走的，不会实际改变unity场景中的显示/隐藏状况。
