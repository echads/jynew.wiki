我们使用github来进行版本管理和社区协同开发，你可以按照以下步骤来搭建你的开发环境，并向服务器提交你的开发成果。
本文适用于github使用不熟练的人，可以按照步骤搭建开发环境，并向项目进行贡献。

## 1、注册github

* 你需要一个github账号来“说明”你的身份，使用浏览器（建议使用Chrome）访问注册地址：https://github.com/
* 你可能会需要一个“科学上网”的软件来保证你与github连接的稳定，这里我推荐使用[Clash](https://github.com/Fndroid/clash_for_windows_pkg/releases)

> 网友提供的[Clash使用教程](https://loukky.com/archives/1337)


## 2、将项目fork到你的仓库里(Fork)

需要参与项目贡献，你首先需要将本项目“镜像”到你的私人仓库中。

* 访问www.github.com/jynew/jynew ，按下图所示点击

![image](https://user-images.githubusercontent.com/7448857/122948050-319b5b00-d3ad-11eb-9856-594438131c4f.png)

* 稍等片刻后，你会在你的github中看到本项目

![image](https://user-images.githubusercontent.com/7448857/122948760-bc7c5580-d3ad-11eb-9333-4ed70021c87b.png)

> 注意：fork操作相当于在你自己的代码仓库中生成了jynew的一份备份，你有自己这个项目的全部操作权限。但如果需要与本项目同步，你需要定时同步本项目最新的代码和版本，下面会说到。你可以理解一下fork的概念:[Fork a repo](https://docs.github.com/en/get-started/quickstart/fork-a-repo)

## 3、下载项目(Clone)

* 推荐新手使用github提供的可视化客户端[Github Desktop](https://desktop.github.com/)
> 有兴趣的话，你可以详细阅读github desktop自身提供的完备[使用手册](https://docs.github.com/en/desktop)

* 在刚才fork的项目上，点击code - Open With Github Desktop
![image](https://user-images.githubusercontent.com/7448857/122951043-8c35b680-d3af-11eb-9952-40926ba54ae9.png)

* 网页将自动打开Github Desktop客户端软件。选择项目在你本地保存的目录，然后开始漫长的等待吧~（项目比较大，请耐心）

![image](https://user-images.githubusercontent.com/7448857/122951262-bbe4be80-d3af-11eb-82e6-8c95eaef8e01.png)

* 等这个读条完，就正式下载完毕了！

![image](https://user-images.githubusercontent.com/7448857/122951445-de76d780-d3af-11eb-86df-60db9133cd01.png)

* 正常下载完毕后，你就可以从unity载入项目启动了！详情可参考[准备开始](https://github.com/jynew/jynew/wiki/1.1%E5%87%86%E5%A4%87%E5%BC%80%E5%A7%8B)

## 4、更新同步项目(Pull/Fetch)

你可以通过下面操作来将原始库（jynew/jynew）更新同步到你fork的仓库（你的id/jynew）。

> 建议每次开始“玩弄”本工程的时候，都与最新代码同步。不管您是否打算向本项目提交贡献，都可以保持实时同步最新版本以修复BUG、添加内容等。

* 将远端源库（jynew/jynew）同步到本地文件夹
![image](https://user-images.githubusercontent.com/7448857/122962596-28fc5200-d3b8-11eb-8639-022d62cde100.png)
![image](https://user-images.githubusercontent.com/7448857/122962227-c5722480-d3b7-11eb-914a-b1b294d873c7.png)
![image](https://user-images.githubusercontent.com/7448857/122962312-df136c00-d3b7-11eb-8ba7-ef7375d828b1.png)

* 将本地文件夹上传到fork的仓库（你的id/jynew）
![image](https://user-images.githubusercontent.com/7448857/122962525-16821880-d3b8-11eb-8e23-bc870f4cd789.png)

## 5、向项目提交你的修改(Pull Request)

* 修改本地文件后，可以在Github Desktop中看到相关修改
![image](https://user-images.githubusercontent.com/7448857/122963151-9b6d3200-d3b8-11eb-8d2a-141ba9f5bb2b.png)

* 勾选你要提交的文件，点击Commit to main
![image](https://user-images.githubusercontent.com/7448857/122963286-b50e7980-d3b8-11eb-83e7-1495ab67acbd.png)

* 推送到远端仓库（你的id/jynew）
![image](https://user-images.githubusercontent.com/7448857/122963395-ceafc100-d3b8-11eb-8eb8-39cfbffdbc8f.png)

* 创建pull request
![image](https://user-images.githubusercontent.com/7448857/122963616-061e6d80-d3b9-11eb-89f9-a4572f5965ae.png)

* 在弹出的网页上勾选提交的内容，填写提交信息，之后等待本项目管理员审核完毕后即可合并入游戏主干代码。
![image](https://user-images.githubusercontent.com/7448857/122964353-c015d980-d3b9-11eb-81f9-e036f0050375.png)

![)C%TEJH(8ATZRSC)TARH449](https://user-images.githubusercontent.com/7448857/122964416-d459d680-d3b9-11eb-968f-1949891746f7.png)

## 6、解决冲突

有时候你会遇到本地版本与更新版本之间有冲突而报错，这里有一个视频教你如何解决：<br>
[How to resolve a Merge Conflict in GitHub Desktop](https://www.youtube.com/watch?v=MIVW0sijSjY)