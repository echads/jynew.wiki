# 金群3D重制版中的LUA脚本

## 概述

* 本游戏使用[xlua](https://github.com/Tencent/xLua)作为lua的运行库。
* lua脚本位于jyx2\data\lua 目录，其中游戏业务相关的脚本位于jygame子目录内
* 每一个业务脚本文件对应一个事件，文件名为 ka[脚本id].lua
* [lua扩展语法](https://github.com/jynew/jynew/wiki/1.4.1%E9%87%8D%E5%88%B6%E7%89%88%E6%89%A9%E5%B1%95Lua%E8%AF%AD%E6%B3%95)

## 游戏事件与lua脚本对应关系
所有的事件都是使用lua脚本来进行驱动，可以在工程中jyx2\data\lua\jygame目录内看到，文件名为 ka[脚本id].lua

建议使用Notepad++/sublime来进行编辑。

如之前提到的98号事件，对应的就是ka98.lua

![图片14](https://user-images.githubusercontent.com/7448857/119354542-c8a1c400-bcd6-11eb-91c8-58812c19c07d.png)

也可以在upedit4kyscpp中查看，

![图片15](https://user-images.githubusercontent.com/7448857/119354556-cc354b00-bcd6-11eb-889b-8247df1248bf.png)

Lua代码即为对应事件的“翻译”，所以对应函数也可以在编辑器中查看具体的参数功能。


## 如何扩展lua指令

你可以扩展一条lua指令，具体步骤：

- 在jyx2\data\lua\main.lua里注册一条指令
- 在jyx2\Assets\Scripts\LuaCore\Jyx2LuaBridge.cs中实现
- xlua/Generate Wrap File（生成lua wrap文件，具体请见xlua相关文档）

## 调试lua

使用·键（1左边的键）打开控制台，可以输入lua指令进行调试
![图片16](https://user-images.githubusercontent.com/7448857/119355133-7ca34f00-bcd7-11eb-9d09-1ee11aed95fc.png)
