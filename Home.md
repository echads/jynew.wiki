`由于项目正在大规模重构中，以下部分文档已过期。敬请期待重构完成后更新文档。`


### 通用篇
* [1.1准备开始](https://github.com/jynew/jynew/wiki/1.1%E5%87%86%E5%A4%87%E5%BC%80%E5%A7%8B)
  * [1.1.1配置表机制简介](https://github.com/jynew/jynew/wiki/1.1.1%E9%85%8D%E7%BD%AE%E8%A1%A8%E6%9C%BA%E5%88%B6%E7%AE%80%E4%BB%8B)
* [1.2游戏运行机制](https://github.com/jynew/jynew/wiki/1.2%E6%B8%B8%E6%88%8F%E8%BF%90%E8%A1%8C%E6%9C%BA%E5%88%B6)
* [1.3事件触发器](https://github.com/jynew/jynew/wiki/%E4%BA%8B%E4%BB%B6%E8%A7%A6%E5%8F%91%E5%99%A8)
* [1.4游戏事件脚本](游戏事件脚本)
  * [1.4.1Lua脚本](https://github.com/jynew/jynew/wiki/Lua%E8%84%9A%E6%9C%AC)
  * [1.4.2蓝图脚本](蓝图脚本)
  * [1.4.3游戏事件指令(API)](游戏事件指令)
* [1.5搭建游戏世界差异解决办法](https://github.com/jynew/jynew/wiki/1.5%E6%90%AD%E5%BB%BA%E6%B8%B8%E6%88%8F%E4%B8%96%E7%95%8C%E5%B7%AE%E5%BC%82%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95)
* [1.6Git使用流程简介](https://github.com/jynew/jynew/wiki/1.6Git%E6%8B%89%E5%8F%96%EF%BC%8C%E6%8F%90%E4%BA%A4%EF%BC%8C%E6%8E%A8%E9%80%81%E6%95%99%E7%A8%8B)

### 游戏内工具篇
* [2.1技能编辑器（可预览模型、动作、武器挂点、技能特效等）](https://github.com/jynew/jynew/wiki/2.1%E6%8A%80%E8%83%BD%E7%BC%96%E8%BE%91%E5%99%A8)
* [2.2控制台](https://github.com/jynew/jynew/wiki/2.2%E6%8E%A7%E5%88%B6%E5%8F%B0)
* [2.3存档生成](https://github.com/jynew/jynew/wiki/2.3%E5%AD%98%E6%A1%A3%E7%94%9F%E6%88%90)
* [2.4战斗调试](https://github.com/jynew/jynew/wiki/2.4%E6%88%98%E6%96%97%E8%B0%83%E8%AF%95)
* [2.5角色模型配置管理器](https://github.com/jynew/jynew/wiki/2.5%E8%A7%92%E8%89%B2%E6%A8%A1%E5%9E%8B%E9%85%8D%E7%BD%AE%E7%AE%A1%E7%90%86%E5%99%A8)

### 程序篇
* [3.1项目代码总览](https://github.com/jynew/jynew/wiki/3.1%E9%A1%B9%E7%9B%AE%E4%BB%A3%E7%A0%81%E6%80%BB%E8%A7%88)
* [3.2资源加载管理机制](https://github.com/jynew/jynew/wiki/3.2%E8%B5%84%E6%BA%90%E5%8A%A0%E8%BD%BD%E7%AE%A1%E7%90%86%E6%9C%BA%E5%88%B6)
* [3.3配置表加载和读取](https://github.com/jynew/jynew/wiki/3.3%E9%85%8D%E7%BD%AE%E8%A1%A8%E5%8A%A0%E8%BD%BD%E5%92%8C%E8%AF%BB%E5%8F%96)
* [3.4存档数据结构](https://github.com/jynew/jynew/wiki/3.4%E5%AD%98%E6%A1%A3%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84)
* [3.5Lua虚拟机](https://github.com/jynew/jynew/wiki/3.5Lua%E8%99%9A%E6%8B%9F%E6%9C%BA)
* [3.6地图流程说明](https://github.com/jynew/jynew/wiki/3.6%E5%9C%B0%E5%9B%BE%E6%B5%81%E7%A8%8B%E8%AF%B4%E6%98%8E)
* [3.7战斗流程说明](战斗流程说明)

### 艺术和资产篇
* 4.1艺术和资产概述（TODO）
* [4.2人物立绘制作标准](https://github.com/jynew/jynew/wiki/4.2%E4%BA%BA%E7%89%A9%E7%AB%8B%E7%BB%98%E5%88%B6%E4%BD%9C%E6%A0%87%E5%87%86)
* [4.3人物三维模型和动作制作标准](https://github.com/jynew/jynew/wiki/4.3%E4%BA%BA%E7%89%A9%E5%8A%A8%E4%BD%9C%E5%88%B6%E4%BD%9C%E6%A0%87%E5%87%86)
* 4.4技能特效制作标准（TODO）
* 4.5三维场景和道具制作标准（TODO）
* 4.6UI标准（TODO）
* 4.7音频标准（TODO）

### 项目篇
* 5.1我们如何制定ROADMAP(TODO)
* 5.2如何协作和参与开发(TODO)