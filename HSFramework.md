**HSFramework**是[汉家松鼠工作室](https://www.hanjiasongshu.com)开发的一套Unity游戏开发工具集。

在本项目中未开源，存放目录位于jyx2/Assets/HanSquirrel.DLL/*

请勿用于本项目外任何其他用途。

如果你对它有兴趣，可以联系邮箱地址： hanjiasongshu@163.com



