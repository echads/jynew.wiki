## [GEN DATA]是做什么？

游戏中一些玩家数据是用excel表格存储的（具体位置在jyx2\excel），GEN DATA这个操作是将excel表格内容生成供程序读取的二进制数据。
所以每次修改excel内容（或者拉取更新内容）后，请运行一次。

普通的GEN DATA和强制更新区别是，是否增量/全部更新，如果存在配置表的格式修改，则需要强制更新。
在不确定是否需要强制的情况下，点强制更新总不会错，但需要花费更多的时间。

更多配置表相关信息，可以参阅[配置表相关开发文档](https://github.com/jynew/jynew/wiki/3.3%E9%85%8D%E7%BD%AE%E8%A1%A8%E5%8A%A0%E8%BD%BD%E5%92%8C%E8%AF%BB%E5%8F%96)


## 配置表生成

游戏逻辑使用到的excel表格，我们统称为配置表。

游戏通过[reg](https://github.com/jynew/jynew/tree/main/jyx2/reg)文件夹中xml的文件描述配置表到代码中数据结构的生成规则

每类配置表都可以映射到一个[pojo](https://github.com/jynew/jynew/tree/main/jyx2/Assets/Scripts/Jyx2Pojos)类

仿照本工程中的写法，即可自由扩展配置表数据
