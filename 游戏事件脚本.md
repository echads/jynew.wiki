## 概述

本游戏中游戏具体执行逻辑叫做`游戏事件脚本`（以下也称`事件脚本`、`事件`），比如对话、获得物品、开启一场战斗等。

每个事件对应一个整数编号，在[事件触发器](https://github.com/jynew/jynew/wiki/%E4%BA%8B%E4%BB%B6%E8%A7%A6%E5%8F%91%E5%99%A8)中填入。

游戏事件脚本可以使用[Lua脚本](https://github.com/jynew/jynew/wiki/Lua%E8%84%9A%E6%9C%AC)或[蓝图脚本](蓝图脚本)实现，如果一个脚本同时定义有lua脚本和蓝图脚本，则执行蓝图脚本。

我们推荐使用蓝图进行脚本实现，因为蓝图具有强的差错性以及便利的编辑工具。

## 事件脚本样例

以一个典型的脚本为例子，如100号事件

* Lua

```lua
Talk(15, "你又想做什么？", "talkname15", 0);
if AskBattle() == true then goto label0 end;
    do return end;
::label0::
    Talk(0, "晚辈斗胆向前辈讨教讨教．", "talkname0", 1);
    Talk(15, "好，我们来玩玩．", "talkname15", 0);
    if TryBattle(132) == false then goto label1 end;
        LightScence();
        Talk(15, "小子，过些时候，我金花婆婆再向你讨教．", "talkname15", 0);
        Talk(0, "我会等您的．", "talkname0", 1);
        do return end;
::label1::
        LightScence();
        Talk(15, "看你资质挺好的，老婆婆我不想杀你，你走吧．", "talkname15", 0);
do return end;
```

* 蓝图

![image](https://user-images.githubusercontent.com/7448857/123723236-91699880-d8bc-11eb-841f-52921865d090.png)
