## 蓝图脚本

本游戏开发框架提供可视化编程模式，你可以对任何一个事件进行蓝图节点编写而不需要编写对应代码。蓝图脚本与[Lua脚本](Lua脚本)具有完全等价的功能。

> 特别注意：本游戏的蓝图脚本与虚幻引擎（unreal）里的蓝图系统没有关系，仅都是使用图形化编程来实现开发效率的提升，请勿对号入座。

## 创建蓝图脚本

创建一个蓝图脚本，你可以在菜单项 `项目快速导航/游戏事件脚本/蓝图脚本` 打开对应目录，在空白处右键选择 `Create > Jyx2 Node Graph` 进行新建，文件名即对应脚本id。

## 蓝图脚本基本操作

双击蓝图脚本文件即可打开编辑界面，你可以`鼠标右键`进行添加节点，亦可拖拽相关节点位置，填入参数。将相邻的节点连接起来（`prev`和`next`节点）

注意：

* 每个图只允许有一个没有`前向节点（prev）`的节点，它将作为事件的开始节点。
* 逻辑遇到没有`后向节点（next）`的节点运行完即结束事件逻辑。

## 一个典型的蓝图样例

![image](https://user-images.githubusercontent.com/7448857/123723236-91699880-d8bc-11eb-841f-52921865d090.png)



